load('api_mqtt.js');
load('api_gpio.js');
load('api_timer.js');
load('api_config.js');
load('api_dash.js');
load('api_adc.js');

Timer.set(1000 , true, function() {			
  let a_data = ADC.read(0);
  let chick = ADC.enable(0);
  let res = MQTT.pub('code', JSON.stringify({ data: a_data }), 1);
  print('Published : ', res ? 'yes' : 'no'); 
  print("data :: ", a_data, "::" , chick );

}, null);
